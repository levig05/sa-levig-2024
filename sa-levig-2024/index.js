import express from "express";
import { router } from "./backend/data/data.routes.js"; //
import bodyParser from "body-parser";
import cookieParser from "cookie-parser";


const app = express();
app.use(bodyParser.json());


app.use(cookieParser());

/*
app.get('/', (req, res) => {
  // Lese die Benutzer-ID aus dem Cookie oder erstelle eine neue
  const userId = req.cookies.userId || generateUniqueId();
  
  // Setze das Cookie mit der Benutzer-ID
  res.cookie('userId', userId, { maxAge: 900000, httpOnly: true });
  const userCoordinates = req.cookies.userCoordinates || { latitude: 0, longitude: 0 };
  console.log(userCoordinates)

  
});*/



app.use(express.static("frontend"));

app.use(express.json());
app.use("/api", router); //

app.listen(3001, () => {
  console.log("Server listens to http://localhost:3001");
});


function generateUniqueId() {
  // Implementiere eine Funktion zur Generierung einer eindeutigen ID
  return Math.random().toString(36).substring(7);
}
