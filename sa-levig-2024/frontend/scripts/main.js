const selectCity = document.getElementById("selectCity");
const body = document.getElementById("codeBody");
const elLine = document.getElementById("line");
const elBar = document.getElementById("bar");
const headIcon = document.getElementById("headIcon");

// Koordinaten aller Auswählbaren Orte
const orte = [
  {
    suchName: "romanshorn",
    actualName: "Romanshorn",
    geolocation: {
      latitude: 47.5635,
      longitude: 9.3564,
    },
  },
  {
    suchName: "weinfelden",
    actualName: "Weinfelden",
    geolocation: {
      latitude: 47.5698,
      longitude: 9.112,
    },
  },
  {
    suchName: "frauenfeld",
    actualName: "Frauenfeld",
    geolocation: {
      latitude: 47.5558,
      longitude: 8.8964,
    },
  },
  {
    suchName: "kreuzlingen",
    actualName: "Kreuzlingen",
    geolocation: {
      latitude: 47.6458,
      longitude: 9.1783,
    },
  },
  {
    suchName: "M%C3%BCnchwilen",
    actualName: "Münchwilen",
    geolocation: {
      latitude: 47.47719,
      longitude: 8.99677,
    },
  },
];

let dataTest = {
  address: "romanshorn, TG",
  currentConditions: {
    cloudcover: 94.1,
    conditions: "Overcast",
    icon: "snow",
    temp: 1.5,
    windspeed: 12.9,
  },
  days: [
    {
      cloudcover: 82.1,
      conditions: "Rain, Partially cloudy",
      description:
        "Partly cloudy throughout the day with a chance of rain throughout the day.",
      icon: "rain",
      temp: 6.9,
      windspeed: 31.4,
      hours: [
        {
          temp: 8.5,
          windspeed: 25.8,
          precip: 0.016,
        },
      ],
      precip: 0.782,
    },
  ],
};

let nearestLocation;
let data;

function calculateDistance(lat1, lon1, lat2, lon2) {
  const R = 6371; // Radius der Erde in Kilometern
  const dLat = deg2rad(lat2 - lat1);
  const dLon = deg2rad(lon2 - lon1);

  const a =
    Math.sin(dLat / 2) * Math.sin(dLat / 2) +
    Math.cos(deg2rad(lat1)) *
      Math.cos(deg2rad(lat2)) *
      Math.sin(dLon / 2) *
      Math.sin(dLon / 2);

  const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
  const distance = R * c; // Entfernung in Kilometern

  return distance;
}

function deg2rad(deg) {
  return deg * (Math.PI / 180);
}

async function getCurrentPosition() {
  return new Promise((resolve, reject) => {
    navigator.geolocation.getCurrentPosition(resolve, reject);
  });
}

async function findNearestLocation(userCoords) {
  let minDistance = Infinity;
  let nearestLocation = null;

  for (const location of orte) {
    const distance = calculateDistance(
      userCoords.latitude,
      userCoords.longitude,
      location.geolocation.latitude,
      location.geolocation.longitude
    );

    if (distance < minDistance) {
      minDistance = distance;
      nearestLocation = location;
    }
  }

  return nearestLocation;
}

async function main() {
  try {
    const userCoords = (await getCurrentPosition()).coords;
    //console.log("User Coordinates:", userCoords);

    nearestLocation = await findNearestLocation(userCoords);
    //console.log("Nearest Location:", nearestLocation);
  } catch (error) {
    console.error("Error getting user location:", error);
  }
  selectCity.value = orte.indexOf(nearestLocation);

  await getData(nearestLocation.suchName);
}

function getData(ort) {
  axios.get("api/city/" + ort).then((response) => {
    data = response.data;
    console.log(data);
    format();
  });
}

const info = document.querySelectorAll(".info div");
const tempText = document.getElementsByClassName("temp")[0];
const rainText = document.getElementsByClassName("rain")[0];
const iconEl = document.getElementById("icon");

//defaut img:
iconEl.src = "../resources/sunny.png";

//console.log(info);
let selectedValue = document.querySelector('input[name="days"]:checked').value;

//console.log(selectedValue)     -1
function getCurrentHourIndex(data) {
  const currentHour = getCurrentHour(); // Holt die aktuelle Stunde
  const hours = data.days[0].hours; // Holt die hours aus dem data-Objekt (hier nur der erste Tag betrachtet)

  // Iteriere über die Stunden und finde den Index der Stunde, die der aktuellen Stunde entspricht
  for (let i = 0; i < hours.length; i++) {
    const hour = parseInt(hours[i].datetime.split(":")[0]);
    if (hour === currentHour) {
      return i; // Gib den Index zurück, wenn die Stunde übereinstimmt
    }
  }

  return -1; // Gib -1 zurück, wenn die aktuelle Stunde nicht gefunden wurde
}

function getCurrentHour() {
  const now = new Date();
  return now.getHours(); // Gibt die aktuelle Stunde als Zahl zurück
}

function format() {
  //console.log(data)
  //body.innerHTML = "Hello World"
  headIcon.href =
    "./resources/" +
    data.days[1].hours[getCurrentHourIndex(data)].icon +
    ".png";
  selectedValue = document.querySelector('input[name="days"]:checked').value;
  //console.log(selectedValue);
  iconEl.src = "../resources/" + data.days[selectedValue - 1].icon + ".png";
  if (selectedValue - 1 === 1) {
    //getCurrentHourIndex(data)
    tempText.innerHTML =
      data.days[selectedValue - 1].hours[getCurrentHourIndex(data)].temp + "°C";
    rainText.innerHTML =
      data.days[selectedValue - 1].hours[getCurrentHourIndex(data)].precip +
      "% Regen";
  } else {
    //iconEl.src = "../resources/" + data.days[selectedValue - 1].icon + ".png";
    tempText.innerHTML = data.days[selectedValue - 1].temp + "°C";
    //info[1].innerHTML = data.days[selectedValue - 1].conditions;
    rainText.innerHTML = data.days[selectedValue - 1].precip + "% Regen";
  }
  updateChart();
}

selectCity.addEventListener("change", async (e) => {
  //console.log(e.target.value) => Zahl
  await getData(orte[e.target.value].suchName);
});

function getTempForDayArray() {
  selectedValue = document.querySelector('input[name="days"]:checked').value;
  let temps = data.days[selectedValue - 1].hours.map((dat) => dat.temp);
  let rains = data.days[selectedValue - 1].hours.map((dat) => dat.precip * 100);
  let arr = [];
  arr.push(temps);
  arr.push(rains);
  //console.log(arr)
  return arr;
}

//console.log("Wetter am " + new Date())

let lab = [];
for (let p = 0; p < 24; p++) {
  let labTransfer;
  if (p < 10) {
    labTransfer = "0" + p + ":00";
  } else {
    labTransfer = "" + p - (p % 10) + (p % 10) + ":00";
  }
  lab.push(labTransfer);
}
//console.log(lab)
let startValue = [];
for (let m = 0; m < 24; m++) {
  startValue.push(0);
}

let dataLine = {
  labels: lab,
  datasets: [
    {
      label: "Temperatur",
      data: startValue,
      fill: true,
      borderColor: "rgb(255, 192, 203)",
      tension: 0.5,
    },
  ],
};

let dataBar = {
  labels: lab,
  datasets: [
    {
      label: "Niederschlag in %",
      data: startValue,
      fill: true,
      borderColor: "rgb(77, 187, 230)",
      backgroundColor: "rgb(77, 187, 230)",
      tension: 0.5,
    },
  ],
};

const configLine = new Chart(elLine, {
  type: "line",
  data: dataLine,
});

const configBar = new Chart(elBar, {
  type: "bar",
  data: dataBar,
});

function updateChart() {
  //dataLine
  let x = getTempForDayArray();
  //temp
  dataLine.datasets[0].data = x[0];

  //rain
  dataBar.datasets[0].data = x[1];

  configLine.data.datasets = dataLine.datasets;
  configBar.data.datasets = dataBar.datasets;
  configLine.update();
  configBar.update();
}

const choseDay = document.getElementById("choseDay");
choseDay.addEventListener("change", format);

await main();
