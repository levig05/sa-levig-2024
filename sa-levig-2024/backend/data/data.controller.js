import {
  getData
} from "./data.model.js";

async function getWeatherData(request, response) {
  //console.log(request.params.ort)
  const data = await getData(request.params.ort);

  response.json(data);
}

export {
  getWeatherData
};
