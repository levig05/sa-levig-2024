import { Router } from "express";
import {
  getWeatherData
} from "./data.controller.js";

const router = Router();

router.get("/city/:ort", getWeatherData);


export { router };
