import axios from "axios";

const baseURLst =
  "https://weather.visualcrossing.com/VisualCrossingWebServices/rest/services/timeline/";
//dazwischen Ortsname mit grossem anfangsbuchstabe          WICHTIG! Münchwilen = M%C3%BCnchwilen
let url = "";

const aktuellesDatum = new Date();

// Ein Tag abziehen
let dateMin = new Date(aktuellesDatum);
dateMin.setDate(aktuellesDatum.getDate() - 1);

// Drei Tage hinzufügen
let dateMax = new Date(aktuellesDatum);
dateMax.setDate(aktuellesDatum.getDate() + 3);

//console.log(dateMin.toLocaleDateString());  // Beispiel: "07.01.2024"
//console.log(dateMax.toLocaleDateString());  // Beispiel: "11.01.2024"

//let place = "romanshorn"

const midURL = "%2C%20TG/";

// 2024-01-04/2024-01-08

const baseURLend =
  "?unitGroup=metric&key=J9N7BN4YS5SGRH4L7P94RAQSR&contentType=json";
let dateTransferMin = "" + dateMin;
let dateTransferMax = "" + dateMax;

function umwandelnDatumFormat(inputString) {
  // Ein Date-Objekt aus dem Eingabestring erstellen
  const datumObjekt = new Date(inputString);

  // Datumkomponenten extrahieren
  const jahr = datumObjekt.getFullYear();
  const monat = ("0" + (datumObjekt.getMonth() + 1)).slice(-2); // Monat beginnt bei 0, daher +1
  const tag = ("0" + datumObjekt.getDate()).slice(-2);

  // Das Format "YYYY-MM-DD" erstellen
  const umgewandeltesDatum = `${jahr}-${monat}-${tag}`;

  return umgewandeltesDatum; // Ausgabe: "2024-01-07"
}

dateMin = umwandelnDatumFormat(dateTransferMin);
dateMax = umwandelnDatumFormat(dateTransferMax);

//const inputString = "Sun Jan 07 2024 08:51:50 GMT+0100 (Mitteleuropäische Normalzeit)";
//const umgewandeltesDatum = umwandelnDatumFormat(inputString);

const dateString = dateMin + "/" + dateMax;

function getData(ort) {
  url = baseURLst + ort + midURL + dateString + baseURLend;
  //console.log(dateString)
  //console.log(url)
  //console.log(dateString)
  //console.log(dateString.slice(0,15))
  return axios.get(url).then((response) => {
    //console.log(response)
    const allData = response.data;
    return allData;
  });
}

export { getData };
